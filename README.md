Требования
==========
 - PHP 5.5
 - Apache 2.4
 - MySQL 5

Установка
=========
 - Настроить vhost Apache `/etc/apache2/sites-enabled/sdek.conf`
```
<VirtualHost *:80>
    ServerName sdek.loc

    DocumentRoot /vagrant/sdek/web
    <Directory /vagrant/sdek/web>
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog /var/log/apache2/sdek.log
    CustomLog /var/log/apache2/sdek.log combined
</VirtualHost>
```
 - Установить `composer`:
```bash
curl -sS https://getcomposer.org/installer | php
```
 - Установить зависимости
```bash
php composer.phar install
```
 - Настроить параметры `app/config/parameters.yml`:
```yaml
parameters:
    database_driver: pdo_mysql
    database_host: 127.0.0.1
    database_port: 3306
    database_name: sdek
    database_user: root
    database_password: root
    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: null
    mailer_password: null
    locale: en
    secret: ThisTokenIsNotSoSecretChangeIt
```
 - Создать БД и таблицы:
```bash
./app/console doctrine:database:create
./app/console doctrine:schema:update --force
```
 - Залить фикстуры:
```bash
./app/console doctrine:fixtures:load
```

Использование
=============
 - Полный список категорий `http://sdek.loc/app_dev.php/`
   ![all](http://imagizer.imageshack.com/img537/4310/lWuUQF.png)
 - Раскрывающийся список категорий `http://sdek.loc/app_dev.php/tree`
   ![tree](http://imagizer.imageshack.com/img538/3987/Xp8cmN.png)