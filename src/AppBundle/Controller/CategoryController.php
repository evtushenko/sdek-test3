<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Category;

class CategoryController extends Controller
{
    /**
     * @return Response
     *
     * @Route("/", name="category_all")
     */
    public function allAction()
    {
        $categories = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository(Category::class)
            ->childrenHierarchy();

        return $this->render('category/all.html.twig', ['categories' => $categories]);
    }

    /**
     * @return Response
     *
     * @Route("/tree", name="category_tree")
     */
    public function treeAction()
    {
        return $this->render('category/tree.html.twig');
    }

    /**
     * @param int|null $id
     * @return Response
     *
     * @Route("/children", name="category_roots")
     * @Route("/children/{id}", name="category_children", requirements={"id"="\d+"})
     */
    public function childrenAction($id = null)
    {
        if ($id === null) {
            $parent = null;
        } else {
            $parent = $this->container->get('doctrine.orm.entity_manager')
                ->getRepository(Category::class)
                ->find($id);
            if ($parent === null) {
                throw $this->createNotFoundException('Category not found');
            }
        }

        $children = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository(Category::class)
            ->getChildren($parent, true);
        $children = array_map(function(Category $category) {
            return [
                'id'        => $category->getId(),
                'title'     => $category->getTitle(),
                'is_leaf'   => $category->isLeaf(),
            ];
        }, $children);

        return new JsonResponse($children);
    }
}
