<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\Category;

/**
 */
class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    const MAX_DEPTH = 5;
    const MIN_CHILDREN = 2;
    const MAX_CHILDREN = 3;

    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @inheritdoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @inheritdoc
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0, $n = mt_rand(self::MIN_CHILDREN, self::MAX_CHILDREN); $i < $n; ++$i) {
            $this->createCategoryTree($manager);
        }

        $manager->flush();
    }

    private function createCategoryTree(ObjectManager $manager, Category $parent = null, $depth = 0)
    {
        $category = new Category();
        $category->setParent($parent);
        $category->setTitle($this->generateCategoryName());
        $manager->persist($category);

        if ($depth < self::MAX_DEPTH) {
            for ($i = 0, $n = mt_rand(self::MIN_CHILDREN, self::MAX_CHILDREN); $i < $n; ++$i) {
                $this->createCategoryTree($manager, $category, $depth + 1);
            }
        }
    }

    private function generateCategoryName()
    {
        return $this->container->get('faker.generator')->word();
    }

    /**
     * @inheritdoc
     */
    public function getOrder()
    {
        return 1;
    }
}
