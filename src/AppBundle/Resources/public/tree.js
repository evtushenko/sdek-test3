(function($, w, d) {
    'use strict';

    var htmlEncode = function(text) {
        return $('<div/>').text(text).html();
    };
    var drawChildren = function(children) {
        var html = '<ol class="children">';
        $.each(children, function(i, category) {
            html += '<li>';
            if (!category.is_leaf) {
                html += '<a href="#" class="expand-tree" data-id="' + category.id + '">';
                html += htmlEncode(category.title);
                html += '</a>';
            } else {
                html += htmlEncode(category.title);
            }

            html += '</li>';
        });
        html += '</ol>';

        return html;
    };

    w.initTree = function(base_url) {
        var loadChildren = function (parent_id) {
            return $.ajax({
                type: 'get',
                url: base_url + (parent_id ? '/' + parent_id : ''),
                dataType: 'json',
                cache: true,

                error: function() {
                    w.alert('Ошибка загрузки');
                }
            });
        };

        $(d).on('click', '.expand-tree', function (e) {
            e.preventDefault();

            var $link = $(e.currentTarget);
            if ($link.data('loading')) {
                return;
            }

            if ($link.data('loaded')) {
                $link.siblings('.children').toggle();
                return;
            }

            $link.data('loading', true);

            loadChildren($link.data('id'))
                .done(function (data) {
                    $link.data('loading', false);
                    $link.data('loaded', true);

                    $link.after(drawChildren(data));
                })
                .fail(function () {
                    $link.data('loading', false);
                    $link.data('loaded', false);
                });
        });

        loadChildren(null).done(function (data) {
            $('#category-tree').html(drawChildren(data));
        });
    };
})(jQuery, window, document);